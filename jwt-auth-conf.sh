export VAULT_ADDR="https://my-vault-cluster.hashicorp.cloud:8200"
export VAULT_NAMESPACE="admin"
export VAULT_TOKEN=""


vault auth enable -path gitlab jwt
vault write auth/gitlab/config \
  jwks_url="https://gitlab.com/-/jwks" \
  bound_issuer="https://gitlab.com"


# Submit the production policy for the `main` GitLab branch
vault policy write demo-prod main.policy

# Submit the dev policy for all other branches
vault policy write demo-dev dev.policy

vault write auth/gitlab/role/vault-gitlab-ci-demo-main -<<EOF
{
  "role_type": "jwt",
  "policies": ["demo-prod"],
  "token_explicit_max_ttl": 600,
  "user_claim": "user_email",
  "bound_claims": {
    "project_path": "sofixa1/vault-gitlab-ci",
    "ref": "main",
    "ref_type": "branch"
  }
}
EOF

vault write auth/gitlab/role/vault-gitlab-ci-demo-dev -<<EOF
{
  "role_type": "jwt",
  "policies": ["demo-dev"],
  "token_explicit_max_ttl": 600,
  "user_claim": "user_email",
  "bound_claims": {
    "project_path": "sofixa1/vault-gitlab-ci",
    "ref": "dev",
    "ref_type": "branch"
  }
}
EOF
